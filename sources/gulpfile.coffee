'use strict'
gulp 		= require 'gulp'
gp         	= (require 'gulp-load-plugins') lazy: false
path 		= require 'path'
browserify 	= require 'browserify'
stylus		= require 'gulp-stylus'
sourcemap	= require 'gulp-sourcemaps'
source 		= require 'vinyl-source-stream'
livereload 	= require 'gulp-livereload'
streamify 	= require 'gulp-streamify'
uglify 		= require 'gulp-uglify'
gulpif 		= require 'gulp-if'
args		= require('yargs').argv
concat		= require 'gulp-concat'
minifyCss	= require 'gulp-minify-css'
gutil 		= require 'gulp-util'
buffer		= require 'vinyl-buffer'
urlAdjuster = require 'gulp-css-url-adjuster'
file		= require 'gulp-file'
# plumber		= require 'gulp-plumber'

APP_ENV = if args.prod then true else false


paths =
	build : '../Application/front/'
	images: 'static/images/**/*'
	json: 'static/json/**/*'
	vendors: 'static/vendors/**/*'

#########################################################
# HTML
#########################################################
gulp.task 'html', ->
	gulp.src 'src/jade/index.jade'
	.pipe gp.plumber()
	.pipe gp.jade
		locals: pageTitle: 'Gulp Boilerplate'
		pretty: args.prod == undefined
	.pipe gulp.dest paths.build + '../'
	.pipe livereload()


#########################################################
# JS
#########################################################
libs = [
	'jquery'
	'underscore'
	'backbone'
	'handlebars'
	'sugar'
]

gulp.task 'js:libs', ->
	b = browserify
		paths: ['./node_modules', './vendors']
		extensions: ['.js']
		debug: true
	.require libs
	.transform 'debowerify'
	.transform 'deamdify'
	.bundle()
	.pipe source 'libs.js'
	.pipe streamify uglify()
	.pipe gulp.dest paths.build + 'js'


gulp.task 'js', ->
	b = browserify
		entries: ['./src/coffee/app.coffee']
		extensions: ['.coffee']
		debug: true
	.external libs
	.on "error", (error) -> console.log error
	.transform 'browserify-replace',
		replace: [
			from:/\$APPENV/
			to: APP_ENV
		]
	.transform 'coffeeify'
	.transform 'deamdify'
	.transform 'uglifyify'
	.bundle()
	.on 'error', (err)->
		gutil.log 'Browserify Error', err
		# gulp.src ['www/js'], read: false
		# 	.pipe gp.clean force: true
		b.end()
		livereload.reload()
		file 'bundle.js', 'console.log("'+String(err)+'")'
			.pipe gulp.dest paths.build + 'js'
	.pipe source 'bundle.js'
	.pipe gulpif args.prod, streamify uglify()
	.pipe gulp.dest paths.build + 'js'
	.pipe livereload()


#########################################################
#CSS
#########################################################
gulp.task 'css', ->
	gulp.src ['vendors/knacss.css', 'src/styl/main.styl']
		.pipe gp.plumber()
		.pipe stylus
			compress: true
		.pipe urlAdjuster
			prepend: '../../../../front/images/'
		.pipe concat 'main.css'
		.pipe sourcemap.init()
		.pipe gulpif args.prod, minifyCss()
		.pipe sourcemap.write()
		.pipe gulp.dest paths.build + 'css'
		.pipe livereload()

# gulp.task 'css:print', ->
# 	gulp.src ['src/styl/print/print.styl']
# 		.pipe gp.plumber()
# 		.pipe stylus
# 			compress: true
# 		.pipe urlAdjuster
# 			prepend: '../../../../front/images/'
# 		.pipe concat 'print.css'
# 		.pipe sourcemap.init()
# 		.pipe gulpif args.prod, minifyCss()
# 		.pipe sourcemap.write()
# 		.pipe gulp.dest paths.build + 'css'
# 		.pipe livereload()

#########################################################
# IMAGES
#########################################################
gulp.task 'static-images', ->
	gulp.src paths.images
		.pipe gulp.dest paths.build + 'images'
		.pipe livereload()

#########################################################
# JSON
#########################################################
gulp.task 'static-json', ->
	gulp.src paths.json
		.pipe gulp.dest paths.build + 'json'
		.pipe livereload()

gulp.task 'static-vendors', ->
	gulp.src paths.vendors
		.pipe gulp.dest paths.build + 'vendors'
		.pipe livereload()

#########################################################
# Clean
#########################################################
gulp.task 'clean', ->
	gulp.src [
		paths.build + 'index.html'
		paths.build + 'images'
		paths.build + 'css'
		paths.build + 'js'
		paths.build + 'json'
		paths.build + 'vendors'
		'tmp'
	], read: false
		.pipe gp.clean force: true

#########################################################
# Build
#########################################################
gulp.task 'build', ['static-images', 'static-json', 'static-vendors', 'html', 'js', 'css']

#########################################################
# Default task
#########################################################
gulp.task 'default', ['clean', 'js:libs'], -> gulp.start 'build'

#########################################################
# Connect
#########################################################
gulp.task 'connect', ['build'], ->
	gp.connect.server
		root: paths.build + '../'
		port: 8080
		livereload: true
		# host: '192.168.0.36'
#########################################################
# Watch
#########################################################
gulp.task 'watch', ['connect'], ->
	livereload.listen()
	gulp.watch 'src/jade/**/*.jade', ['html']
	gulp.watch 'src/coffee/**/*.coffee', ['js']
	gulp.watch 'src/styl/**/*.styl', ['css']
	gulp.watch 'static/images/**/*', ['static-images']
	gulp.watch 'static/json/**/*', ['static-json']
	gulp.watch 'static/vendors/**/*', ['static-vendors']
