Build
-----
	
	#Installer Gulp si besoin
	npm install -g gulp

	#installer les packages
	npm install

	#nettoyer le répertoire de destination
	gulp clean

	#transpiler les librairies/vendors
	gulp js:libs

	#watch
	gulp watch

	#transpiler la prod
	gulp --prod

