$ = window.jQuery = window.$ = require 'jquery'

_ = require 'underscore'
Backbone = require 'backbone'
Backbone.$ = $

Router = require './Router'

Config = require './Config'

Main = require './Main'




$ ->
	window.app = {
		dispatcher : _.clone(Backbone.Events)
		constants : 
			LOADED: 'loaded'
		router: new Router
		config : new Config
		ENV_PROD : $APPENV
	}

	console.log window.app.ENV_PROD

	new Main
