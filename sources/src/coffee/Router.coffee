Backbone = require 'backbone'
#/////////////////////////////////////////////////////
# class
#/////////////////////////////////////////////////////
module.exports = Backbone.Router.extend
	
	routes:
		'' 				: 'main'

	main:->
		@trigger 'change', 
			page: 'main'

