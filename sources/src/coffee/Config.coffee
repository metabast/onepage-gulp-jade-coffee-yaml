$			= require 'jquery'
_			= require 'underscore'
Handlebars	= require 'handlebars'
#/////////////////////////////////////////////////////
# class
#/////////////////////////////////////////////////////
module.exports = class

	constructor:->

		Handlebars.registerHelper 'toUpperCase', (str)->
			if str and typeof str is 'string'
				new Handlebars.SafeString str.toUpperCase()
			else
				'*'

		
